module.exports = {
  bracketSpacing: true,
  jsxBracketSameLine: true,
  singleQuote: false,
  trailingComma: "none",
  semi: false,
  tabWidth: 2
}
