/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { useState, useEffect } from "react"
import Geolocation from "@react-native-community/geolocation"
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Alert,
  ImageBackground
} from "react-native"
import { Button } from "react-native-elements"

const API_KEY = "AIzaSyBDqDc8k_9HkiTE7sccHm9L1qnveV5dBm0" // Use your own API key if you want to test the app. This one is not valid anymore

const image = {
  uri:
    "https://66.media.tumblr.com/39e7a0085e9c2a5e87f70f89e259147c/tumblr_ow23gvvY2F1wrt8hqo1_1280.jpg"
}

const imageButte = {
  uri:
    "https://preview.redd.it/vofwg2cq7zy31.jpg?auto=webp&s=e5a357914a6f3b2be134ed03747d61d62336b0af"
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    marginHorizontal: 30,
    height: 245
  },
  button: {
    marginVertical: 20,
    marginHorizontal: 30
  },
  meme: {
    marginHorizontal: 30,
    height: 455
  },
  image: {
    flex: 1,
    width: 354,
    height: 250,
    resizeMode: "stretch"
  },
  imageButte: {
    flex: 1,
    width: 354,
    height: 455,
    resizeMode: "stretch"
  },
  text: {
    flex: 1,
    fontSize: 50,
    color: "white"
  }
})

declare const global: { HermesInternal: null | {} }

type Coordinates = {
  lat: number | null
  lng: number | null
}

type Leg = {
  duration: {
    value: number
    text: string
  }
}

enum Truth {
  YES = "Yes",
  NO = "No",
  MAYBESO = "MaybeSO"
}

const createThreeButtonAlert = (result: Truth) => {
  Alert.alert(
    result,
    "",
    [
      {
        text: "cool"
      },
      {
        text: "nice"
      },
      {
        text: "rate 5 stars"
      }
    ],
    { cancelable: false }
  )
}

const getTime = async (shops: Array<String>, location: Coordinates) => {
  let bestTime = Number.MAX_VALUE
  for (let i = 0; i < shops.length; i++) {
    const shop = shops[i]

    const reqURL = `https://maps.googleapis.com/maps/api/directions/json?origin=${location.lat},${location.lng}&destination=place_id:${shop}&key=${API_KEY}`
    const data = await fetch(reqURL, {
      method: "GET",
      headers: {
        "X-Ios-Bundle-Identifier": "org.reactjs.native.example.YesNoMaybeSO"
      }
    })

    const json = await data.json()
    const legValues: Array<number> = json.routes[0].legs.map(
      (leg: Leg) => leg.duration.value
    )
    const value = legValues.reduce((a, b) => a + b, 0) // The route consists of legs that each have their own duration, they will be summed together to get the total duration

    if (value < bestTime) {
      bestTime = value
    }
  }
  return bestTime
}

const App = () => {
  const [location, setLocation] = useState<Coordinates>({
    lat: null,
    lng: null
  })

  const [shops, setShops] = useState<String[]>([])

  const [reloads, setReloads] = useState(0)

  const calculate = async () => {
    const date = new Date()

    const duration = (await getTime(shops, location)) * 1000

    const newDate = new Date(date.getTime() + duration)

    if (
      (newDate.getHours() === 20 && newDate.getMinutes() > 55) ||
      (newDate.getHours() === 21 && newDate.getMinutes() < 3)
    ) {
      return Truth.MAYBESO
    } else if (newDate.getHours() >= 21 || newDate.getHours() < 9) {
      return Truth.NO
    }

    return Truth.YES
  }

  useEffect(() => {
    Geolocation.getCurrentPosition((info) => {
      const coords = info.coords
      setLocation({ lat: coords.latitude, lng: coords.longitude })
    })
  }, [reloads])

  useEffect(() => {
    const reqURL = `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${location.lat},${location.lng}&rankby=distance&type=convenience_store&key=${API_KEY}`
    fetch(reqURL, {
      method: "GET",
      headers: {
        "X-Ios-Bundle-Identifier": "org.reactjs.native.example.YesNoMaybeSO"
      }
    })
      .then((res) => {
        res.json().then((json) => {
          const places: Array<any> = json.results
          setShops(places.slice(0, 4).map((place) => place.place_id))
        })
      })
      .catch((err) => console.error(err))
  }, [location])

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView>
          <View style={styles.container}>
            <ImageBackground source={image} style={styles.image}>
              <Text style={styles.text}>Hello YesNoMaybeSO</Text>
            </ImageBackground>
          </View>
          <View style={styles.button}>
            <Button
              title="Do I have time to buy beer? (Click here to maybe find out)"
              raised={true}
              type="outline"
              onPress={async () => {
                setReloads(reloads + 1)
                try {
                  const reality = await calculate()
                  createThreeButtonAlert(reality)
                } catch (err) {
                  console.error(err)
                }
              }}
            />
          </View>
          <View style={styles.meme}>
            <ImageBackground source={imageButte} style={styles.imageButte} />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  )
}

export default App
