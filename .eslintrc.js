module.exports = {
  root: true,
  extends: "@react-native-community",
  parser: "@typescript-eslint/parser",
  plugins: ["@typescript-eslint"],
  rules: {
    "comma-dangle": ["error", "never"],
    quotes: ["error", "double"],
    semi: ["error", "never"]
  }
}
